#!/bin/bash

# Argument handling
if [ $# -ne 2 ]; then
    echo "Usage: $0 <username> <domain>"
    exit 1
fi

USERNAME=$1
DOMAIN=$2

# Generate a random password
PASSWORD=$(openssl rand -base64 16)

# Install necessary packages
dnf update -y
yum groupinstall -y "Development Tools"
dnf install -y epel-release wget nano libxml2-devel libxslt-devel gd-devel libwebp-devel libjpeg-turbo-devel libpng-devel openssl-devel 

# Manually configure MariaDB 10.11 for maximum performance
cat >/etc/yum.repos.d/MariaDB10.11.repo <<EOF

# MariaDB 10.11 RedHatEnterpriseLinux repository list - created 2023-12-16 07:08 UTC
# https://mariadb.org/download/
[mariadb]
name = MariaDB
# rpm.mariadb.org is a dynamic mirror if your preferred mirror goes offline. See https://mariadb.org/mirrorbits/ for details.
# baseurl = https://rpm.mariadb.org/10.11/rhel/8.7/x86_64/
baseurl = https://mirrors.gigenet.com/mariadb/yum/10.11/rhel/8.7/x86_64/
module_hotfixes = 1
# gpgkey = https://rpm.mariadb.org/RPM-GPG-KEY-MariaDB
gpgkey = https://mirrors.gigenet.com/mariadb/yum/RPM-GPG-KEY-MariaDB
gpgcheck = 1
EOF

dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm
yes | dnf module reset php
yes | dnf module enable php:remi-8.2
dnf install -y git MariaDB-server redis php php-fpm php-mysqlnd php-json php-opcache php-gd php-curl php-mbstring php-xml php-imagick php-zip php-intl php-cli php-devel certbot brotli brotli-devel 

# Manually gather nginx for Brotli support
NGINX_VERSION=$(curl -s https://nginx.org/en/download.html | grep -Po 'Mainline version.*?nginx-([0-9.]+)\.tar' | cut -d '-' -f 2).gz
NGINX_DIR="/usr/local/src/nginx-`echo $NGINX_VERSION | sed 's/.tar.gz//'`"
NGINX_CONFIG="--prefix=/etc/nginx
              --sbin-path=/usr/sbin/nginx
              --modules-path=/usr/lib/nginx/modules
              --conf-path=/etc/nginx/nginx.conf
              --error-log-path=/var/log/nginx/error.log
              --http-log-path=/var/log/nginx/access.log
              --pid-path=/var/run/nginx.pid
              --lock-path=/var/run/nginx.lock
              --http-client-body-temp-path=/var/cache/nginx/client_temp
              --http-proxy-temp-path=/var/cache/nginx/proxy_temp
              --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp
              --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp
              --http-scgi-temp-path=/var/cache/nginx/scgi_temp
              --user=nginx
              --group=nginx
              --with-compat
              --with-file-aio
              --with-threads
              --with-http_addition_module
              --with-http_auth_request_module
              --with-http_dav_module
              --with-http_flv_module
              --with-http_gunzip_module
              --with-http_gzip_static_module
              --with-http_mp4_module
              --with-http_random_index_module
              --with-http_realip_module
              --with-http_slice_module
              --with-http_ssl_module
              --with-http_sub_module
              --with-http_v2_module
              --with-mail
              --with-mail_ssl_module
              --with-stream
              --with-stream_realip_module
              --with-stream_ssl_module
              --with-stream_ssl_preread_module
              --with-pcre
              --with-pcre-jit
              --with-http_stub_status_module
              --with-http_secure_link_module
              --with-http_image_filter_module
              --with-http_xslt_module
              --add-module=/usr/local/src/ngx_brotli
              --add-module=/usr/local/src/ngx_cache_purge-2.3"

# Download and extract nginx source
cd /usr/local/src
wget https://nginx.org/download/nginx-$NGINX_VERSION
tar -xvf nginx-$NGINX_VERSION

# Download and extract ngx_brotli module
git clone https://github.com/google/ngx_brotli.git
cd ngx_brotli
git submodule update --init

# Add the fastcgi cache clear module
cd ..
wget http://labs.frickle.com/files/ngx_cache_purge-2.3.tar.gz
tar xfvz ngx_cache_purge-2.3.tar.gz

# Configure and build nginx with ngx_brotli module
cd $NGINX_DIR
./configure $NGINX_CONFIG
make
make install
mkdir -p /var/cache/nginx/client_temp
mkdir -p /var/cache/nginx/wordpress
chown -R nginx:nginx /var/cache/nginx/
cat <<EOF > /usr/lib/systemd/system/nginx.service
[Unit]
Description=The NGINX HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target
 
[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -c /etc/nginx/nginx.conf
ExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.conf
ExecReload=/usr/sbin/nginx -s reload
ExecStop=/usr/sbin/nginx -s stop
 
[Install]
WantedBy=multi-user.target
EOF

# php redis
wget https://github.com/phpredis/phpredis/archive/refs/tags/5.3.4.tar.gz
tar xzf 5.3.4.tar.gz
cd phpredis-5.3.4
phpize
./configure
make
make install
echo "extension=redis.so" > /etc/php.d/redis.ini
systemctl restart php-fpm

# Start the required services
systemctl enable --now nginx php-fpm mariadb redis

# Configure MariaDB for WordPress
mariadb-secure-installation <<EOF

y
$PASSWORD
$PASSWORD
y
y
y
y
EOF

mysql -uroot -p$PASSWORD <<EOF
CREATE DATABASE ${USERNAME}_wp;
CREATE USER '${USERNAME}_wp'@'localhost' IDENTIFIED BY '$PASSWORD';
GRANT ALL PRIVILEGES ON ${USERNAME}_wp.* TO '${USERNAME}_wp'@'localhost';
FLUSH PRIVILEGES;
EOF

# Configure PHP-FPM
cp /etc/php-fpm.d/www.conf /etc/php-fpm.d/$USERNAME.conf
sed -i -e "s/user = apache/user = $USERNAME/" \
-e 's/group = apache/group = nginx/' \
-e 's/;listen.owner = nobody/listen.owner = nginx/' \
-e 's/;listen.group = nobody/listen.group = nginx/' \
-e "s/listen = 127.0.0.1:9000/listen = \/run\/php-fpm\/$USERNAME.sock/" \
-e 's/;listen.mode = 0660/listen.mode = 0660/' \
-e "s/\[www\]/[$USERNAME]/" \
-e "s/www-/$USERNAME-/gi" \
-e "s/www.sock/$USERNAME.sock/" /etc/php-fpm.d/$USERNAME.conf

systemctl restart php-fpm

# Create user and directory
useradd -m -d /home/$USERNAME $USERNAME
usermod -a -G nginx $USERNAME
mkdir -p /home/$USERNAME/public/$DOMAIN
mkdir -p /home/$USERNAME/.ssl
chown -R $USERNAME:$USERNAME /home/$USERNAME/public
chgrp -R nginx /etc/nginx
chmod -R g+rwx /etc/nginx

# Configure Nginx for the domain
cat >/etc/nginx/conf.d/$DOMAIN.conf <<EOF
server {
    listen 80;
    server_name $DOMAIN;
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl;
    http2 on;
    server_name $DOMAIN;
    root /home/$USERNAME/public;
    index index.php;

    # SSL
    ssl_certificate /home/$USERNAME/.ssl/$DOMAIN.crt;
    ssl_certificate_key /home/$USERNAME/.ssl/$DOMAIN.key;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout 10m;

    # Security
    server_tokens off;
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-XSS-Protection "1; mode=block" always;
    add_header X-Content-Type-Options "nosniff" always;

    # Upload Rules
    client_max_body_size 1024M;
    client_body_buffer_size 1024M;

    # Enable HTTP/2
    add_header Alternate-Protocol 443:npn-spdy/3;

    # Caching with Nginx Helper
    set \$no_cache 0;

    # POST requests and URLs with a query string should always go to PHP
    if (\$request_method = POST) {
        set \$no_cache 1;
    }
    if (\$query_string != "") {
        set \$no_cache 1;
    }

    # Don't cache PHP requests
    if (\$request_uri ~* "/wp-admin/|/xmlrpc.php|/wp-(app|cron|login|register|mail).php|wp-.*.php|/feed/|index.php|wp-comments-popup.php|wp-links-opml.php|wp-locations.php|sitemap(_index)?.xml|[a-z0-9_-]+-sitemap([0-9]+)?.xml") {
        set \$no_cache 1;
    }

    # Enable caching for logged-in users
    if (\$http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass|wordpress_no_cache|wordpress_logged_in") {
        set \$no_cache 1;
    }

    # Avoid some logs
    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    # Cache static files for 1 hour
    location ~* \.(jpg|jpeg|png|gif|ico|svg|css|js|woff2|woff|ttf|otf|eot)$ {
        expires 1h;
        add_header Pragma public;
        add_header Cache-Control "public";
    }

    # WordPress-specific rules
    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_pass unix:/run/php-fpm/$USERNAME.sock;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_cache_bypass \$no_cache;
        fastcgi_no_cache \$no_cache;
        fastcgi_cache WORDPRESS;
        fastcgi_cache_valid 200 60m;
    }

    # Nginx Helper caching rules
    location ~ ^/wp-content/cache/minify/ {
        expires 1h;
        add_header Pragma public;
        add_header Cache-Control "public";
    }
    location ~ /purge(/.*) {
        fastcgi_cache_purge WORDPRESS "\${scheme}\${request_method}\${host}$1";
        access_log off;
        add_header Content-Type "text/plain";
        return 200 "Cache purged.";
    }
}

EOF
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.original
cat >/etc/nginx/nginx.conf <<EOF
user nginx;
worker_processes auto;
pid /run/nginx.pid;
worker_rlimit_nofile 65535;

events {
    worker_connections 1024;
    use epoll;
    multi_accept on;
}

http {
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 60;
    types_hash_max_size 2048;
    server_tokens off;

    # MIME types
    include mime.types;
    default_type application/octet-stream;

    # Logging
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    # SSL configuration
    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout 10m;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-CHACHA20-POLY1305:AES256-GCM-SHA384:CHACHA20-POLY1305:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!KRB5:!SRP:!DSS;

    # Optimizations
    client_header_timeout 10;
    client_body_timeout 10;
    send_timeout 10;
    keepalive_requests 100000;
    reset_timedout_connection on;
    server_names_hash_max_size 2048;
    server_names_hash_bucket_size 128;
    client_header_buffer_size 1k;
    large_client_header_buffers 2 1k;
    fastcgi_cache_path /var/cache/nginx/wordpress levels=1:2 keys_zone=WORDPRESS:100m inactive=60m;
    fastcgi_cache_key "$scheme$request_method$host$request_uri";

    # Security headers
    add_header X-Content-Type-Options nosniff;
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-Xss-Protection "1; mode=block";
    add_header Referrer-Policy "same-origin";
    add_header X-Permitted-Cross-Domain-Policies none;
    add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload" always;

    # Include site configurations
    include /etc/nginx/conf.d/*.conf;
}
EOF

systemctl restart nginx
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# Install and configure WordPress using WP-CLI
wp core download --path=/home/$USERNAME/public --allow-root
# Install and configure WordPress using WP-CLI
wp config create --path=/home/$USERNAME/public --dbname=${USERNAME}_wp --dbuser=${USERNAME}_wp --dbpass=$PASSWORD --allow-root
wp core install --path=/home/$USERNAME/public --url=https://$DOMAIN --title="My WordPress Site" --admin_user=$USERNAME --admin_password=$PASSWORD --admin_email=your-email@example.com --allow-root

echo "define('FS_METHOD', 'direct');" >> /home/$USERNAME/public/wp-config.php
chown -R $USERNAME:$USERNAME /home/$USERNAME/public
chmod og+x /home/$USERNAME

# Enable WP-CLI for the new user
mkdir /home/$USERNAME/.wp-cli
cp -r /root/.wp-cli/* /home/$USERNAME/.wp-cli/
chown -R $USERNAME:$USERNAME /home/$USERNAME/.wp-cli

# Grant sudo permissions to the web user for systemctl command
echo "$USERNAME ALL=(ALL) NOPASSWD: /bin/systemctl" > /etc/sudoers.d/$USERNAME

# Obtain SSL certificate from Let's Encrypt
systemctl stop nginx
certbot certonly --standalone -d $DOMAIN --non-interactive --agree-tos -m noreply@mynode.host --cert-name $DOMAIN --preferred-challenges http --http-01-port 80 --deploy-hook "cp /etc/letsencrypt/live/$DOMAIN/fullchain.pem /home/$USERNAME/.ssl/$DOMAIN.crt && cp /etc/letsencrypt/live/$DOMAIN/privkey.pem /home/$USERNAME/.ssl/$DOMAIN.key && chown $USERNAME:$USERNAME /home/$USERNAME/.ssl/*"
systemctl start nginx

# Output any relevant information
echo "WordPress installation completed. You can access it at https://$DOMAIN."
echo "Admin username: $USERNAME"
echo "Admin password: $PASSWORD"
echo "MySQL Root: $PASSWORD"